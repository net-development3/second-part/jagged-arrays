﻿using System;

#pragma warning disable S2368

namespace JaggedArrays
{
    public static class ArrayExtension
    {
        public static void OrderByAscendingBySum(this int[][] source)
        {
            if (source == null)
            {  
                throw new ArgumentNullException(nameof(source)); 
            }

            for (int i = 0; i < source.Length - 1; i++)
            {
                for (int j = 0; j < source.Length - i - 1; j++)
                {
                    if (SumCalculation(source[j]) > SumCalculation(source[j + 1]))
                    {
                        int[] buffer = source[j];
                        source[j] = source[j + 1];
                        source[j + 1] = buffer;
                    }
                }
            }
        }

        public static void OrderByDescendingBySum(this int[][] source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            for (int i = 0; i < source.Length - 1; i++)
            {
                for (int j = 0; j < source.Length - i - 1; j++)
                {
                    if (SumCalculation(source[j]) < SumCalculation(source[j + 1]))
                    {
                        int[] buffer = source[j];
                        source[j] = source[j + 1];
                        source[j + 1] = buffer;
                    }
                }
            }
        }
        
        public static void OrderByAscendingByMax(this int[][] source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            for (int i = 0; i < source.Length - 1; i++)
            {
                for (int j = 0; j < source.Length - i - 1; j++)
                {
                    if (MaxSearch(source[j]) > MaxSearch(source[j + 1]))
                    {
                        int[] buffer = source[j];
                        source[j] = source[j + 1];
                        source[j + 1] = buffer;
                    }
                }
            }
        }

        public static void OrderByDescendingByMax(this int[][] source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            for (int i = 0; i < source.Length - 1; i++)
            {
                for (int j = 0; j < source.Length - i - 1; j++)
                {
                    if (MaxSearch(source[j]) < MaxSearch(source[j + 1]))
                    {
                        int[] buffer = source[j];
                        source[j] = source[j + 1];
                        source[j + 1] = buffer;
                    }
                }
            }
        }

        private static int SumCalculation(int[] array)
        {
            if (array == null)
            {
                return int.MinValue;
            }

            int sum = 0;

            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }

            return sum;
        }

        private static int MaxSearch(int[] array)
        {
            if (array == null)
            {
                return int.MinValue;
            }

            int max = array[0];

            for (int i = 0; i < array.Length; i++)
            {
                if (max < array[i])
                {
                    max = array[i];
                }
            }

            return max;
        }
    }
}
